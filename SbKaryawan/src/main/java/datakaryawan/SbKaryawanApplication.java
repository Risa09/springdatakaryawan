package datakaryawan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbKaryawanApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbKaryawanApplication.class, args);
	}

}
