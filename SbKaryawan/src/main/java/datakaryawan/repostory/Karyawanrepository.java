package datakaryawan.repostory;

import datakaryawan.entity.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Karyawanrepository extends JpaRepository <Karyawan, Integer>{
    Karyawan findByName(String nama);
}
