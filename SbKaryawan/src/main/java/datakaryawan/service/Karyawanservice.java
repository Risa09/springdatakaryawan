package datakaryawan.service;
import datakaryawan.entity.Karyawan;
import datakaryawan.repostory.Karyawanrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class Karyawanservice {
    @Autowired
    private Karyawanrepository repo;

    public Karyawan saveKaryawan(Karyawan karyawan) {
        return repo.save(karyawan);
    }

    public List<Karyawan> saveKaryawans(List<Karyawan> karyawan) {
        return repo.saveAll(karyawan);
    }

    public List<Karyawan> getKaryawans() {
        return repo.findAll();
    }

    public Karyawan getKaryawansById(int id) {
        return repo.findById (id).orElse ( null );
    }

    public Karyawan getKaryawansByName(String nama) {
        return repo.findByName (nama);
    }

    public String deleteKaryawan(Integer id) {
        repo.deleteById(id);
        return "Product remover!!";
    }
}
//    @Autowired
//    private Karyawanrepository repositorykaryawan;
//
//    public Karyawan saveKaryawan(Karyawan karyawan){
//        return repositorykaryawan.save(karyawan);
//    }
//    public List<Karyawan> saveKaryawans(List<Karyawan> karyawans){
//        return repositorykaryawan.saveAll(karyawans);
//    }
//    public List<Karyawan> getKaryawans(){
//        return repositorykaryawan.findAll();
//    }
//
//    public Karyawan getProductsByName(String nama){
//        return repositorykaryawan.findByName(nama);
//    }
//    public String deleteKaryawan(Integer Id){
//        repositorykaryawan.deleteById(Id);
//        return "User Delete!!";
//    }


