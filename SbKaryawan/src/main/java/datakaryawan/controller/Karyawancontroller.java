package datakaryawan.controller;


import datakaryawan.entity.Karyawan;
import datakaryawan.service.Karyawanservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin (origins = "*",allowedHeaders = "*")
@RestController
public class Karyawancontroller {
    @Autowired
    private Karyawanservice service;

    @PostMapping("/addKaryawan")
    public Karyawan addKaryawan(@RequestBody Karyawan karyawan) {
        return service.saveKaryawan ( karyawan );
    }

    @PostMapping("/addKaryawans")
    public List<Karyawan> addKaryawans(@RequestBody List<Karyawan> karyawans) {
        return service.saveKaryawans ( karyawans );
    }

    @GetMapping("/karyawans")
    public List<Karyawan> findAllKaryawans() {
        return service.getKaryawans ();
    }

    @GetMapping("/karyawan/{id}")
    public Karyawan findById(@PathVariable int id) {
        return service.getKaryawansById (id);
    }

    @GetMapping("/karyawanByName/{nama}")
    public Karyawan findProductByName(@PathVariable String nama) {
        return service.getKaryawansByName ( nama );
    }

    @DeleteMapping("/delete/{d}")
    public String deleteKaryawan(@PathVariable int id){
        return service.deleteKaryawan (id);
    }

//    @RequestMapping("/edit/{Id}")
//    public Karyawan editKaryawan(@PathVariable int Id){
//        return service.getKaryawans( Id );
//    }

//        @GetMapping("/karyawans/{nama}")
//        public Karyawan findKaryawanByName (@PathVariable String nama){
//            return service.getProductsByName ( nama );
//        }


//    }
}